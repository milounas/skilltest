// set up ======================================================================
var express  = require('express');
var app      = express();
var session  = require('express-session');
var passport = require('./config/passport');
var mongoose = require('mongoose'); 				// mongoose for mongodb
var port = process.env.PORT || 8080; 				// set the port
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

// configuration ===============================================================
mongoose.connect(process.env.DB_URL); // Connect to local MongoDB instance. 

// middleware ==================================================================
app.use(express.static(__dirname + '/public')); 	// set the static files location /public
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({'extended': 'true'})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request
app.use(session({ 
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true
})); 
app.use(passport.initialize());
app.use(passport.session());

// routes ======================================================================
var routes = {
    app: require('./app/routes')(passport),
    api: require('./app/api')(passport)
};

app.use('/', routes.app);
app.use('/api', routes.api);

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port " + port);