angular.module('skillTest', ['ngRoute', 'mainController', 'investmentService', 'userService'])
.config( ['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'views/overview.html',
			controller: 'OverviewController',
			resolve:{
	            investments: function(InvestmentsFactory){
					// when landing on the page, get all investments
					return InvestmentsFactory.getAll();
	            },
	            user: function(UserFactory){
					// when landing on the page, get the user data
					return UserFactory.getUser();
	            }
			}
		})
		.when('/investment', {
			templateUrl: 'views/investment.html',
			controller: 'InvestmentController',
			resolve:{
				investment: function(InvestmentsFactory, $location){
					return InvestmentsFactory.getById($location.search().id);
				}
			}
		});
}]);
