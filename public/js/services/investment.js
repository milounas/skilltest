angular.module('investmentService', [])

	// super simple service
	// each function returns a promise object
	.factory('InvestmentsFactory', ['$http', '$q',function($http, $q) {
		return {
			getById : function(id) {
				var defer = $q.defer();
				$http.get('/api/investments/'+id).then(function(response){
					if(response.status == 200)
						defer.resolve(response.data);
					else
						defer.reject(response.data);
				});
				return defer.promise;
			},
			getAll: function(){
				var defer = $q.defer();
				$http.get('/api/investments').then(function(response){
					if(response.status == 200)
						defer.resolve(response.data);
					else
						defer.reject(response.data);
				});
				return defer.promise;
			},
			create : function(investmentData) {
				return $http.post('/api/investments', investmentData);
			},
			edit : function(investmentData) {
				return $http.post('/api/investments/'+investmentData.id, investmentData);
			},
			delete : function(id) {
				return $http.delete('/api/investments/' + id);
			}
		};
	}]);