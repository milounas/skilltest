angular.module('userService', [])

	// super simple service
	// each function returns a promise object
	.factory('UserFactory', ['$http', '$q',function($http, $q) {
		return {
			getUser : function() {
				var defer = $q.defer();
				$http.get('/api/user').then(function(response){
					if(response.status == 200)
						defer.resolve(response.data);
					else
						defer.reject(response.data);
				});
				return defer.promise;
			}
		};
	}]);