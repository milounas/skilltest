angular.module('mainController', [])

	// Main controller
	.controller('OverviewController', [
		'$location','$scope','$http','InvestmentsFactory', 'UserFactory',
		'investments', 'user',
	function($location, $scope, $http, InvestmentsFactory, UserFactory, investments, user) {
		$scope.formData = {};
		$scope.loading = true;
		$scope.investments = investments;
		$scope.user = user;

		// DELETE ==================================================================
		// delete an investment after checking it
		$scope.deleteInvestment = function(id) {
			$scope.loading = true;

			InvestmentsFactory.delete(id)
				// if successful deletion, call our get function to get all the new investments
				.success(function(data) {
					$scope.loading = false;
					$scope.investments = data; // assign our new list of investments
				});
		};

		// EVALUATE ================================================================
		// Calculate the size of the portefolio
		$scope.evaluatePortefolio = function() {
            var total = 0;
            for(var i = 0; i < $scope.investments.length; i++){
                var product = $scope.investments[i];
                total += (product.valorisation);
            }
            return total;
		};
		
		// GO TO ===================================================================
		// Redirect to a new view
		$scope.goToInvestment = function(id){
			$location.search({'id': id});
			$location.path('investment');
		};
	
		// LOGOUT ===================================================================
		// Redirect to a new view
		$scope.logout = function(){
			window.location.href = '/logout';
		};
		
	}])
	.controller('InvestmentController', [
		'$location','$scope','$http','InvestmentsFactory', 'UserFactory', 'investment',
	function($location, $scope, $http, InvestmentsFactory, UserFactory, investment) {
		$scope.formData = {};
		$scope.loading 	= true;
		$scope.investment = investment;
		$scope.isEditing = false;

		//Populate the form if we are editing an existing investment
		if(Object.keys($scope.investment).length){
			$scope.isEditing = true;
			$scope.formData = {
				id		: $scope.investment._id,
				title	: $scope.investment.title,
				details	: $scope.investment.details,
				valorisation: $scope.investment.valorisation
			};
		}

		// CREATE ==================================================================
		// when submitting the add form, send the data to the node API
		$scope.createInvestment = function() {

			// Title is a mandatory field
			if ($scope.formData.title != undefined && !isNaN($scope.formData.valorisation)) {
				$scope.loading = true;

				// call the create function from our service (returns a promise object)
				InvestmentsFactory.create($scope.formData)

					// if successful creation, call our get function to get all the new investments
					.success(function(data) {
						$scope.loading = false;
						$scope.formData = {}; // clear the form so our user is ready to enter another
						$scope.investments = data; // assign our new list of investments
						$scope.cancel();
					});
			}
		};
		// EDIT ==================================================================
		// when submitting the edit form, send the data to the node API
		$scope.editInvestment = function() {

			// Title is a mandatory field
			if ($scope.formData.title != undefined) {
				$scope.loading = true;

				// call the create function from our service (returns a promise object)
				InvestmentsFactory.edit($scope.formData)

					// if successful creation, call our get function to get all the new investments
					.success(function(data) {
						$scope.loading = false;
						$scope.formData = {}; // clear the form so our user is ready to enter another
						$scope.investments = data; // assign our new list of investments
						$scope.cancel();
					});
			}
		};
		// Cancel ==================================================================
		// return to the previous page
	    $scope.cancel = function() {
	      window.history.back();
	    };
	}]);