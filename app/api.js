var Investment = require('./models/investment');
var express = require('express');
var router = express.Router();

module.exports = function(passport) {

// ========================================
// API ====================================
// ========================================

// CRUD operations on the user collection
router.route('/user')
	.get(isLoggedIn, function(req, res) {
		res.json(req.user.google);
	});

// CRUD operations on the investment collection
router.route('/investments')
	.get(isLoggedIn, function(req, res) {
		// use mongoose to get the investments in the database
		Investment.find({
			userId: req.user._id
		}, function(err, investments) {
			sendResponseAsJson(res, err, investments);
		});
	})
	.post(isLoggedIn, function(req, res) {
		// create a investment, information comes from AJAX request from Angular
		Investment.create({
			title : req.body.title,
			details : req.body.details,
			valorisation : req.body.valorisation,
			userId: req.user._id
		}, function(err, investment) {
			if (err){
				res.send(err);
			}else{
				// get and return all the investments after you create another
				Investment.find({ userId: req.user._id }, function(err, investments) {
					sendResponseAsJson(res, err, investments);
				});
			}
		});

});

// CRUD operations on one investment
router.route('/investments/:investment_id')
	.get(isLoggedIn, function(req, res) {
		// use mongoose to get the investments in the database
		Investment.findById(req.params.investment_id, function(err, investment) {
			sendResponseAsJson(res, err, investment);
		});
	})
	.post(isLoggedIn, function(req, res) {
		// use mongoose to get the investments in the database
		Investment.findById(req.params.investment_id, function(err, investment) {
			// if there is an error retrieving, send the error. nothing after res.send(err) will execute
			if (err){
				res.send(err);
			}else{
				investment.title = req.body.title;
				investment.details = req.body.details;
				investment.valorisation = req.body.valorisation;
				// get and return all the investments after you create another
				investment.save(function(err){
					if (err){
						res.send(err);
					}
					Investment.find({ userId: req.user._id }, function(err, investments) {
						sendResponseAsJson(res, err, investments);
					});
				});
			}
		});
	})
	.delete(isLoggedIn, function(req, res) {
		Investment.remove({
			_id : req.params.investment_id,
			userId: req.user._id
		}, function(err, investment) {
			if (err){
				res.send(err);
			}else{
				// get and return all the investments after you deleted a investment
				Investment.find({ userId: req.user._id }, function(err, investments) {
					sendResponseAsJson(res, err, investments);
				});
			}
		});
	});

return router;

};

// route middleware to make sure the user is authenticated
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated()){
		return next();
	}
	// if they aren't redirect them to the home page
	res.redirect('/');
}

//Response send as json
function sendResponseAsJson(res, err, data){
	// if there is an error retrieving, send the error. nothing after res.send(err) will execute
	if (err){
		console.error(err);
		res.json({});
	}else{
		res.json(data); // return all investments in JSON format
	}
}