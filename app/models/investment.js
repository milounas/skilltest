var mongoose = require('mongoose'), Schema = mongoose.Schema;

var investmentSchema = mongoose.Schema({
	title : String,
	details: String,
	valorisation: Number,
	userId: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

// create the model for investment and expose it to our app
module.exports = mongoose.model('Investment', investmentSchema);