var path = require('path');
var express = require('express');
var router = express.Router();

module.exports = function(passport) {
	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	router.route('/').get(function(req, res) {
		res.sendFile('index.html', { root: path.join(__dirname, '../public')}); // load the single view file
	});

    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their email
    router.route('/auth/google').get(passport.authenticate('google', { scope : ['profile', 'email'] }));

    // the callback after google has authenticated the user
    router.route('/auth/google/callback').get(
        passport.authenticate('google', {
                successRedirect : '/app',
                failureRedirect : '/'
        })
    );

	// ========================================
	// APPLICATION ============================
	// ========================================
	router.route('/app').get(isLoggedIn, function(req, res) {
		res.sendFile('app.html', { root: path.join(__dirname, '../public')}); // load the single view file
	});


	// =====================================
	// LOGOUT ==============================
	// =====================================
	router.route('/logout').get(function(req, res) {
		req.logout();
		res.redirect('/');
	});
	
	return router;
};

// route middleware to make sure the user is authenticated
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated()){
		return next();
	}
	// if they aren't redirect them to the home page
	res.redirect('/');
}